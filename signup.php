<!DOCTYPE html>
<html lang="en" data-theme="halloween">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Basic PHP Form</title>
        <?php include './templates/meta.php'; ?>
    </head>
    <body class="w-full items-center flex h-screen">
        <main class="max-w-screen-xl items-center flex m-auto">
            <div class="py-16">
                <h1 class="text-4xl font-bold text-center">Sign Up</h1>
                <form action="welcome.php" method="post" class="mt-5">
                    <!--  Name -->
                    <div class="flex justify-between w-full gap-x-2">
                        <label class="form-control w-full max-w-md mx-auto">
                            <div class="label">
                                <span class="label-text">First Name</span>
                            </div>
                            <input name="first_name" type="text" placeholder="Type here" class="input input-bordered w-full max-w-xs" />
                        </label>
                        <label class="form-control w-full max-w-md mx-auto">
                            <div class="label">
                                <span class="label-text">Last Name</span>
                            </div>
                            <input name="last_name" type="text" placeholder="Type here" class="input input-bordered w-full max-w-xs" />
                        </label>
                    </div>

                    <!-- Gender -->
                    <div class="w-full mt-2">
                        <h1 class="text-lg font-bold">Gender</h1>
                        <div class="flex gap-x-2">
                            <div class="form-control">
                                <label class="label cursor-pointer space-x-2 pl-0">
                                    <input value="Male" type="radio" name="gender" class="radio checked:bg-red-500" required/>
                                    <span class="label-text">Male</span> 
                                </label>
                            </div>
                            <div class="form-control">
                                <label class="label cursor-pointer space-x-2">
                                    <input value="Female" type="radio" name="gender" class="radio checked:bg-blue-500" required />
                                    <span class="label-text">Female</span> 
                                </label>
                            </div>
                        </div>
                    </div>

                    <!-- Nationality -->
                    <div class="w-full mt-2">
                        <label class="form-control w-full max-w-md">
                            <div class="label">
                                <span class="label-text">Nationality</span>
                            </div>
                            <select name="nationality" class="select select-bordered">
                                <option disabled selected>Pick one</option>
                                <option>Indonesian</option>
                                <option>Singaporean</option>
                                <option>American</option>
                            </select>
                        </label>
                    </div>

                    <!-- Language Spoken -->
                    <div class="mt-2 max-w-xs">
                        <h1 class="text-lg font-bold">Language Spoken</h1>
                        <div class="form-control">
                            <label class="label cursor-pointer justify-start gap-x-2">
                                <input type="checkbox" name="language[]" class="checkbox checkbox-primary" value="Indonesia" />
                                <span class="label-text">Indonesia</span> 
                            </label>
                            <label class="label cursor-pointer justify-start gap-x-2">
                                <input type="checkbox" name="language[]" class="checkbox checkbox-primary" value="English"  />
                                <span class="label-text">English</span> 
                            </label>
                            <label class="label cursor-pointer justify-start gap-x-2">
                                <input type="checkbox" name="language[]" class="checkbox checkbox-primary" value="Sundanese" />
                                <span class="label-text">Sundanese</span> 
                            </label>
                        </div>
                    </div>

                    <!-- Bio -->
                    <div class="w-full mt-2">
                        <label class="form-control">
                            <div class="label">
                                <span class="label-text">Your bio</span>
                            </div>
                            <textarea name="bio" class="textarea textarea-bordered h-24" placeholder="Bio" required></textarea>
                        </label>
                    </div>

                    <button type="submit" class="btn btn-primary btn-block mt-4" style="background-color: oklch(var(--p)) !important;">Sign Up</button>
                </form>
            </div>
        </main>
    </body>
</html>