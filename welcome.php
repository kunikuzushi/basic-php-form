<!DOCTYPE html>
<html lang="en" data-theme="halloween">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Basic PHP Form</title>
        <?php include './templates/meta.php'; ?>
    </head>
    <body class="w-full">
        <main class="max-w-screen-xl mx-auto flex flex-col gap-y-4 py-16 px-20">
            <?php 
                $firstName = htmlspecialchars($_POST["first_name"], ENT_QUOTES, "UTF-8");
                $lastName = htmlspecialchars($_POST["last_name"], ENT_QUOTES, "UTF-8");
                $fullName = $firstName." ".$lastName;
                $gender = htmlspecialchars($_POST["gender"], ENT_QUOTES, "UTF-8") ?? "Not selected";
                $nationality = htmlspecialchars($_POST["nationality"], ENT_QUOTES, "UTF-8") ?? "Unknown";
                $language = $_POST["language"] ?? array("Unknown");
                $bio = htmlspecialchars($_POST["bio"], ENT_QUOTES, "UTF-8");
            ?>
            <div class="bg-center bg-no-repeat bg-cover w-full h-40" style="background-image: url(/assets/Kaveh-ES-Banner.jpg);"></div>
            <h1 class="font-semibold text-2xl text-center">Welcome, <b><?php echo $fullName ?></b>!</h1>
            <h2 class="font-semibold text-xl">Account Information</h2>
            <ul class="list-disc ml-4">
                <li>
                    <p>First Name: <span class="font-semibold"><?php echo $firstName ?></span></p>
                </li>
                <li>
                    <p>Last Name: <span class="font-semibold"><?php echo $lastName ?></span></p>
                </li>
                <li>
                    <p>Gender: <span class="font-semibold"><?php echo $gender ?></span></p>                
                </li>
                <li>
                    <p>Nationality: <span class="font-semibold"><?php echo $nationality ?></span></p>
                </li>
                <li>
                    <p>Language: <span class="font-semibold"><?php echo htmlspecialchars(join(", ", $language), ENT_QUOTES, "UTF-8") ?></span></p>
                </li>
            </ul>
            
            <div class="border-white border-l-4 pl-4">
                <?php echo $bio ?>
            </div>
        </main>
    </body>
</html>